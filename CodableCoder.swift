//
//  CodableCoder.swift
//  CoreDataTest
//
//  Created by Daniil on 23.04.18.
//  Copyright © 2018 RosEvroBank AO. All rights reserved.
//

import Foundation

open class CodableCoder {
    
    private let encoder = DictionaryEncoder()
    private let decoder = DictionaryDecoder()
    // MARK: Options
    
    /// The strategy to use for coding `Date` values.
    public enum DateCodingStrategy {
        case deferredToDate
        case secondsSince1970
        case millisecondsSince1970
        @available(macOS 10.12, iOS 10.0, watchOS 3.0, tvOS 10.0, *)
        case iso8601
        case formatted(DateFormatter)
        case custom(encode: (Date, Encoder) throws -> Void, decode: (Decoder) throws -> Date)
        case special(encode: (Date, CodingKey?) throws -> Any, decode: (Any, CodingKey?) throws -> Date?)
    }
    
    /// The strategy to use for coding `Data` values.
    public enum DataCodingStrategy {
        case deferredToData
        case base64
        case custom(encode: (Data, Encoder) throws -> Void, decode: (Decoder) throws -> Data)
        case special(encode: (Data, CodingKey?) throws -> Any, decode: (Any, CodingKey?) throws -> Data?)
    }
    
    /// The strategy to use for non-Dictionary-conforming floating-point values (IEEE 754 infinity and NaN).
    public enum NonConformingFloatCodingStrategy {
        case `throw`
        case convertString(positiveInfinity: String, negativeInfinity: String, nan: String)
    }
    /// The strategy to use for automatically changing the value of keys before encoding.
    public enum KeyCodingStrategy {
        case useDefaultKeys
        case convertSnakeCase
        case custom(encode: (_ codingPath: [CodingKey]) -> CodingKey, decode: (_ codingPath: [CodingKey]) -> CodingKey)
        case special(encode: (String) -> String, decode: (String) -> String)
    }
    
    /// The strategy to use in encoding dates. Defaults to `.deferredToDate`.
    open var dateCodingStrategy: DateCodingStrategy = .deferredToDate {
        didSet {
            switch dateCodingStrategy {
            case .deferredToDate:
                encoder.dateEncodingStrategy = .deferredToDate
                decoder.dateDecodingStrategy = .deferredToDate
            case .iso8601:
                encoder.dateEncodingStrategy = .iso8601
                decoder.dateDecodingStrategy = .iso8601
            case .millisecondsSince1970:
                encoder.dateEncodingStrategy = .millisecondsSince1970
                decoder.dateDecodingStrategy = .millisecondsSince1970
            case .secondsSince1970:
                encoder.dateEncodingStrategy = .secondsSince1970
                decoder.dateDecodingStrategy = .secondsSince1970
            case .formatted(let formatter):
                encoder.dateEncodingStrategy = .formatted(formatter)
                decoder.dateDecodingStrategy = .formatted(formatter)
            case .custom(let encode, let decode):
                encoder.dateEncodingStrategy = .custom(encode)
                decoder.dateDecodingStrategy = .custom(decode)
            case .special(let encode, let decode):
                encoder.dateEncodingStrategy = .special(encode)
                decoder.dateDecodingStrategy = .special(decode)
            }
        }
    }
    open var dataCodingStrategy: DataCodingStrategy = .base64 {
        didSet {
            switch dataCodingStrategy {
            case .deferredToData:
                encoder.dataEncodingStrategy = .deferredToData
                decoder.dataDecodingStrategy = .deferredToData
            case .base64:
                encoder.dataEncodingStrategy = .base64
                decoder.dataDecodingStrategy = .base64
            case .custom(let encode, let decode):
                encoder.dataEncodingStrategy = .custom(encode)
                decoder.dataDecodingStrategy = .custom(decode)
            case .special(let encode, let decode):
                encoder.dataEncodingStrategy = .special(encode)
                decoder.dataDecodingStrategy = .special(decode)
            }
        }
    }
    open var nonConformingFloatCodingStrategy: NonConformingFloatCodingStrategy = .throw {
        didSet {
            switch nonConformingFloatCodingStrategy {
            case .throw:
                encoder.nonConformingFloatEncodingStrategy = .throw
                decoder.nonConformingFloatDecodingStrategy = .throw
            case .convertString(let positiveInfinity, let negativeInfinity, let nan):
                encoder.nonConformingFloatEncodingStrategy = .convertToString(positiveInfinity: positiveInfinity, negativeInfinity: negativeInfinity, nan: nan)
                decoder.nonConformingFloatDecodingStrategy = .convertFromString(positiveInfinity: positiveInfinity, negativeInfinity: negativeInfinity, nan: nan)
            }
        }
    }
    open var keyCodingStrategy: KeyCodingStrategy = .useDefaultKeys {
        didSet {
            switch keyCodingStrategy {
            case .convertSnakeCase:
                encoder.keyEncodingStrategy = .convertToSnakeCase
                decoder.keyDecodingStrategy = .convertFromSnakeCase
            case .useDefaultKeys:
                encoder.keyEncodingStrategy = .useDefaultKeys
                decoder.keyDecodingStrategy = .useDefaultKeys
            case .custom(let encode, let decode):
                encoder.keyEncodingStrategy = .custom(encode)
                decoder.keyDecodingStrategy = .custom(decode)
            case .special(let encode, let decode):
                encoder.keyEncodingStrategy = .special(encode)
                decoder.keyDecodingStrategy = .special(decode)
            }
        }
    }
    
    open var userInfo: [CodingUserInfoKey : Any] = [:] {
        didSet {
            encoder.userInfo = userInfo
            decoder.userInfo = userInfo
        }
    }
    
    open class func code<T : Encodable, D: Decodable>(_ type: D.Type, from: T) throws -> D {
        if T.self == D.self { return from as! D }
        let encoded = try DictionaryEncoder().encode(from)
        return try DictionaryDecoder().decode(D.self, from: encoded)
    }
    
    open func code<T : Encodable, D: Decodable>(_ type: D.Type, from: T) throws -> D {
        if T.self == D.self { return from as! D }
        let encoded = try encoder.encode(from)
        print(encoded)
        return try decoder.decode(D.self, from: encoded)
    }
    
}

extension Decodable {
    init<T: Encodable>(from: T, coder: CodableCoder = CodableCoder()) throws {
        self = try coder.code(Self.self, from: from)
    }
}
